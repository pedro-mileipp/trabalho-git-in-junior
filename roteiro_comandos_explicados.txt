git help: Mostra vários comandos de forma resumida

git help <comando>: Mostra detalhes de um comando específico

git config --global user.name “Seu Nome”
git config --global user.email “seuemaill@gmail.com”   (Usar o mesmo email do gitlab)
 
Para o repositório atual: --local;
Para o Usuário: --global;
Para o Computador: --system

Opcional:
(primeiro testa code -v)
git config --global core.editor 'code --wait' -> Vscode para editor padrão (precisa do code no PATH)


	Se tudo der certo:
	git config --global -e -> Abre o arquivo de configuração no vscode

-------------------------------------------------------


git init  -> Inicia o Git na pasta selecionada pelo usuário.

git status -> Mostra o estado atual do diretório, mostra os arquivos alterados não submetidos, se estão testados.

git add arquivo/diretório -> Adiciona arquivos novos ou alterados no diretório de trabalho à área de teste do Git
git add --all = git add . -> Adiciona todos os arquivos para área de teste

git commit -m “Primeiro commit” -> Salva mudanças no repositório local e adiciona mensagem de commit.

-------------------------------------------------------

git log -> Mostra todas as alterações feitas em ordem cronológicas
git log arquivo -> Todas as alterações feitas naquele arquivo
git reflog -> Mostra resumidamente os commits

-------------------------------------------------------

git show -> Mostra o conteúdo de todas as commits
git show <commit> -> Mostra o conteúdo especificamente daquela commit

-------------------------------------------------------

git diff -> Compara o que está na área de trabalho e o commit mais recente.
git diff <commit1> <commit2> -> Compara os commits escolhidos.

-------------------------------------------------------

git reset --hard <commit> Todas as alterações em arquivos rastreados sao desfeitas

-------------------------------------------------------

git branch -> Lista todas as branches locais
git branch -r -> Lista todas as branches remotas
git branch -a -> Lista todas as branches locais e remotas
git branch -d <branch_name>  -> Remove uma branch local
git branch -D <branch_name> Força a remoção de um branch local. É necessário, porque pode haver uma branch que ainda não foi feito merge.
git branch -m <nome_novo> Renomea a branch selecionada
git branch -m <nome_antigo> <nome_novo> -> Renomeia uma branch específica localmente que não é a corrente.

-------------------------------------------------------

git checkout <branch_name> -> Usado para mudar a branch selecionada
git checkout -b <branch_name>  -> Usado para criar uma branch e atuar direto nela

-------------------------------------------------------

git merge <branch_name> -> Adiciona uma branch à área principal

-------------------------------------------------------

git clone -> Clona o repositório escolhido.
git pull -> Atualiza repositório local com a última versão da origem da branch remota.
git push -> Faz o envio das mudanças comitadas localmente para a origem da branch rastreada.

-------------------------------------------------------

git remote -v -> Faz a listagem dos servidores remotos que o repositório está usando associados com a URL
git remote add origin <url> -> Adiciona arquivos remotos à locais em uma URL
git remote <url> origin -> Faz a troca da URL do servidor 'origin' para a nova URL informada

--------------------------------------------------------

Documentação do git:
https://git-scm.com/doc

Playlist GIT:
https://www.youtube.com/playlist?list=PLucm8g_ezqNq0dOgug6paAkH0AQSJPlIe

Vídeo sobre Git: 
https://www.youtube.com/watch?v=kB5e-gTAl_s

